using Microsoft.EntityFrameworkCore;

namespace FormulaOneApplication.Models.Contexts
{
    public class FOneTeamDbContext: DbContext
    {
        public DbSet<FOneTeam> FOneTeams { get; set; }
        
        public FOneTeamDbContext(DbContextOptions options)
            : base(options)
        {
        }
    }
}