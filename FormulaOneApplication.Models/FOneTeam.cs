﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FormulaOneApplication.Models
{
    public class FOneTeam
    {
        [Key]
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public int FoundationYear { get; set; }
        public int VictoriesNumber { get; set; }
        public bool Subscribed { get; set; }

        public FOneTeam(){}
        
        public FOneTeam(int teamId, string teamName, int foundationYear, int victoriesNumber, bool subscribed)
        {
            TeamId = teamId;
            TeamName = teamName;
            FoundationYear = foundationYear;
            VictoriesNumber = victoriesNumber;
            Subscribed = subscribed;
        }
    }
}