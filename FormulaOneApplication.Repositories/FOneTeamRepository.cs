using System;
using System.Collections.Generic;
using System.Linq;
using FormulaOneApplication.Models;
using FormulaOneApplication.Models.Contexts;
using FormulaOneApplication.Repositories.Interfaces;
using EntityState = Microsoft.EntityFrameworkCore.EntityState;

namespace FormulaOneApplication.Repositories
{
    public class FOneTeamRepository : IFOneTeamRepository
    {
        private readonly FOneTeamDbContext _context;
        private bool _disposed = false;
        
        public FOneTeamRepository(FOneTeamDbContext context)
        {
            _context = context;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }

        public IEnumerable<FOneTeam> GetElements()
        {
            return _context.FOneTeams.ToList();
        }

        public FOneTeam GetElementById(int elementId)
        {
            var query = from b in _context.FOneTeams
                where b.TeamId == elementId
                select b;
            return query.FirstOrDefault();
        }

        public void InsertElement(FOneTeam element)
        {
            _context.FOneTeams.Add(element);
        }

        public void DeleteElement(int elementId)
        {
            var query = from b in _context.FOneTeams
                where b.TeamId== elementId
                select b;
            _context.FOneTeams.Remove(query.FirstOrDefault() ?? throw new InvalidOperationException());
        }

        public void UpdateElement(FOneTeam element)
        {
            _context.Entry(element).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}