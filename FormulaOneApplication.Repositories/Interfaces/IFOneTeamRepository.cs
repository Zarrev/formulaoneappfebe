using FormulaOneApplication.Models;

namespace FormulaOneApplication.Repositories.Interfaces
{
    public interface IFOneTeamRepository: IBaseRepository<FOneTeam, int>
    {
        //Formally interface
    }
}