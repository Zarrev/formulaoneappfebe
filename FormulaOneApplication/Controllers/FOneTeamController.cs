using System.Collections.Generic;
using FormulaOneApplication.Maps.Interfaces;
using FormulaOneApplication.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace FormulaOneApplication.Controllers
{
    [Route("api/[controller]")]
    public class TeamController : Controller
    {
        private readonly IBaseMap<FOneTeamViewModel> _map;

        public TeamController(IBaseMap<FOneTeamViewModel> map)
        {
            _map = map;
        }

        [HttpGet]
        public IEnumerable<FOneTeamViewModel> Get()
        {
            return _map.GetAll();
        }

        [HttpGet("{id}")]
        public FOneTeamViewModel Get([FromBody] int id)
        {
            return _map.GetElement(id);
        }

        [HttpPost]
        public void Post([FromBody] FOneTeamViewModel team)
        {
            _map.Create(team);
        }

        [HttpPost("{id}")]
        public void Post([FromRoute] int id, [FromBody] FOneTeamViewModel team)
        {
            _map.Update(team);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _map.Delete(id);
        }
    }
}