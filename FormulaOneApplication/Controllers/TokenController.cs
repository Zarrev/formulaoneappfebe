﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Threading.Tasks;
using FormulaOneApplication.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using FormulaOneApplication.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace FormulaOneApplication.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : Controller
    {
        private readonly IConfiguration _config;
        private readonly UserManager<IdentityUser> _userManager;

        public TokenController(IConfiguration config, UserManager<IdentityUser> userManager)
        {
            _config = config;
            _userManager = userManager;
        }


        [AllowAnonymous]
        [HttpPost]
        public dynamic Post([FromBody] LoginViewModel login)
        {
            IActionResult response = Unauthorized();
            var user = Authenticate(login).Result;
            if (user != null)
            {
                var tokenString = BuildToken(user);
                response = Ok(new {token = tokenString});
            }

            return response;
        }

        private string BuildToken(UserViewModel user)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                _config["Jwt:Issuer"],
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private async Task<UserViewModel> Authenticate(LoginViewModel login)
        {
            UserViewModel user = null;
            var userToVerify = await _userManager.FindByNameAsync(login.Username);
            if (userToVerify != null)
            {
                if (await _userManager.CheckPasswordAsync(userToVerify, login.Password))
                {
                    user = new UserViewModel {Name = "admin"};   
                }
            }

            return await Task.FromResult<UserViewModel>(user);
        }
    }
}