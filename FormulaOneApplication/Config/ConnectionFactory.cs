using FormulaOneApplication.Data;
using FormulaOneApplication.Models.Contexts;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FormulaOneApplication.Config
{
    public static class ConnectionFactory
    {
        private static void CreateDbContext<T>(IServiceCollection services) where T : DbContext
        {
            services.AddDbContext<T>(options =>
                options.UseInMemoryDatabase("SQLite")
            );
        }

        public static void CreateApplicationDbContext(IServiceCollection services)
        {
            CreateDbContext<ApplicationDbContext>(services);
            services.AddDefaultIdentity<IdentityUser>()
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>();
        }

        public static void CreateFOneTeamsDbContext(IServiceCollection services)
        {
            CreateDbContext<FOneTeamDbContext>(services);
        }
    }
}