using FormulaOneApplication.Data;
using FormulaOneApplication.Maps;
using FormulaOneApplication.Maps.Interfaces;
using FormulaOneApplication.Repositories;
using FormulaOneApplication.Repositories.Interfaces;
using FormulaOneApplication.Services;
using FormulaOneApplication.Services.Interfaces;
using FormulaOneApplication.ViewModels;
using Microsoft.Extensions.DependencyInjection;

namespace FormulaOneApplication.Config
{
    public static class DependencyInjectionConfig
    {
        public static void AddScope(IServiceCollection services)
        {
            services.AddScoped<IBaseMap<FOneTeamViewModel>, FOneTeamMap>();
            services.AddScoped<IFOneTeamService, FOneTeamService>();
            services.AddScoped<IFOneTeamRepository, FOneTeamRepository>();
        }
    }
}