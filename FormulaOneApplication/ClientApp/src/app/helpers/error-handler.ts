import {Observable, Subject} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandler {

  public errorMsg = '';
  private isError = new Subject<boolean>();

  public errorHandling(error: string): void {
    if (error !== null && error !== undefined && error !== '' && error !== 'null') {
      this.errorMsg = error;
    } else {
      this.errorMsg = '';
    }
    this.isError.next(true);
    setTimeout(() => this.isError.next(false), 5000);
  }

  public isErrorMsg(): Observable<boolean> {
    return this.isError.asObservable();
  }
}
