import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Helpers} from '../../helpers/helper';
import {Subscription} from 'rxjs';
import {MatDrawer} from '@angular/material';
import {delay, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-left-panel',
  templateUrl: './left-panel.component.html',
  styleUrls: ['./left-panel.component.scss']
})
export class LeftPanelComponent implements OnInit, OnDestroy {

  @Input() title: string;
  @Input() sidenav: MatDrawer;
  public authentication = false;
  private subscription: Subscription;

  constructor(private helper: Helpers) {
    this.helper.isAuthenticationChanged().pipe(
      startWith(Helpers.isAuthenticated()),
      delay(0))
      .subscribe((value => this.authentication = value));
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  toggleMenu() {
    this.sidenav.toggle();
  }
}
