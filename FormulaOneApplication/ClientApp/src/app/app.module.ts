import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './layout/body/app.component';

import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatIconModule,
  MatPaginatorModule, MatRippleModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSidenavModule} from '@angular/material/sidenav';
import {HttpClientModule} from '@angular/common/http';
import {LeftPanelComponent} from './layout/left-panel/left-panel.component';
import {LoginComponent} from './components/login/login.component';
import {LogoutComponent} from './components/logout/logout.component';
import {TeamsComponent} from './components/teams/teams.component';
import {MatCardModule} from '@angular/material/card';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TeamOperationDialogComponent} from './components/teams/team-operation-dialog/team-operation-dialog.component';
import {Helpers} from './helpers/helper';
import {TeamService} from './services/team-service/team.service';
import {TokenService} from './services/token-service/token.service';

@NgModule({
  declarations: [
    AppComponent,
    LeftPanelComponent,
    LoginComponent,
    LogoutComponent,
    TeamsComponent,
    TeamOperationDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatCardModule,
    MatProgressSpinnerModule,
    FormsModule,
    BrowserAnimationsModule,
    MatPaginatorModule,
    MatTableModule,
    MatToolbarModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatIconModule,
    MatRippleModule
  ],
  entryComponents: [TeamOperationDialogComponent, TeamsComponent],
  providers: [Helpers, TeamService, TokenService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
