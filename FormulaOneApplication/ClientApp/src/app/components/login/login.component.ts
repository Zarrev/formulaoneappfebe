import {Component, OnDestroy, OnInit} from '@angular/core';
import {Helpers} from '../../helpers/helper';
import {Router} from '@angular/router';
import {TokenService} from '../../services/token-service/token.service';
import {ErrorHandler} from '../../helpers/error-handler';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  public username = 'admin';
  public password = 'f1test2018';
  public isError: boolean;
  public error = '';
  private subscriptions: Subscription[] = [];

  constructor(private helpers: Helpers, private router: Router, private tokenService: TokenService,
              private errorHandler: ErrorHandler) {
    this.subscriptions.push(errorHandler.isErrorMsg().subscribe(value => {
      this.isError = value;
      this.error = errorHandler.errorMsg;
    }));
  }

  ngOnInit() {
  }

  public login(): void {
    const authValues = {username: this.username, password: this.password};
    this.subscriptions.push(this.tokenService.auth(authValues).subscribe(token => {
      this.helpers.setToken(token);
      this.router.navigate(['/teams']);
    }, error => this.errorHandler.errorHandling(error)));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
