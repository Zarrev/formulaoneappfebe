import {Component, OnDestroy, ViewChild} from '@angular/core';
import {TeamService} from '../../services/team-service/team.service';
import {MatDialog, MatDialogRef, MatPaginator, MatTableDataSource} from '@angular/material';
import {Team} from '../../models/team';
import {Subscription} from 'rxjs';
import {TeamOperationDialogComponent} from './team-operation-dialog/team-operation-dialog.component';
import {delay, startWith} from 'rxjs/operators';
import {Helpers} from '../../helpers/helper';
import {ErrorHandler} from '../../helpers/error-handler';

// https://stackblitz.com/angular/peaxbnoeqnm?file=app%2Ftable-pagination-example.ts

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnDestroy {

  public displayedColumns: string[] = ['id', 'name', 'foundation', 'victories', 'subscribed', 'actions'];
  public dataSource: MatTableDataSource<Team>;
  public authentication: boolean;
  public isError: boolean;
  public error = '';
  private subscriptions: Subscription[] = [];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private teamService: TeamService, private helpers: Helpers, public dialog: MatDialog,
              private errorHandler: ErrorHandler) {
    errorHandler.isErrorMsg().subscribe(value => {
      this.isError = value;
      this.error = errorHandler.errorMsg;
    });
    this.checkAuth();
    this.getAndSetTeams();
  }

  private checkAuth() {
    this.helpers.isAuthenticationChanged().pipe(
      startWith(Helpers.isAuthenticated()),
      delay(0))
      .subscribe((value => this.authentication = value));
  }

  private getAndSetTeams() {
    this.teamService.getTeams();
    this.subscriptions.push(this.teamService.allTeams.subscribe(teams => this.initAndKeepSync(teams), error => {
      this.fallBackInit();
      this.errorHandler.errorHandling(error);
    }));
  }

  private initAndKeepSync(teams: Team[]) {
    this.dataSource = new MatTableDataSource<Team>(teams);
    this.dataSource.paginator = this.paginator;
  }

  private fallBackInit() {
    const fallBackTeam = new Team();
    fallBackTeam.name = 'N/A';
    fallBackTeam.foundation = 1999;
    fallBackTeam.victories = 0;
    fallBackTeam.subscribed = true;
    fallBackTeam.id = 0;
    this.initAndKeepSync([fallBackTeam]);
  }

  public delete(id: number) {
    this.teamService.delete(id).subscribe(value => console.log('Delete: ' + value),
      error => this.errorHandler.errorHandling(error), () => this.teamService.getTeams());
  }

  private openDialog(team: Team): MatDialogRef<TeamOperationDialogComponent, any> {
    const dialogRef = this.dialog.open(TeamOperationDialogComponent, {
      width: '250px',
      data: team
    });

    return dialogRef;
  }

  public openCreateDialog(): void {
    this.openDialog(new Team()).afterClosed().subscribe(result => {
      console.log('The create dialog was closed');
      if (result instanceof Team) {
        this.teamService.addTeam(result).subscribe(value => console.log('Delete: ' + value),
          error => this.errorHandler.errorHandling(error), () => this.teamService.getTeams());
      }
    });
  }

  public openEditDialog(team: Team) {
    this.openDialog(team).afterClosed().subscribe(result => {
      console.log('The edit dialog was closed');
      if (result instanceof Team) {
        this.teamService.setTeam(result).subscribe(value => console.log('Edit: ' + value),
          error => this.errorHandler.errorHandling(error), () => this.teamService.getTeams());
      }
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subs => subs.unsubscribe());
  }
}
