import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamOperationDialogComponent } from './team-operation-dialog.component';

describe('TeamCreateDialogComponent', () => {
  let component: TeamOperationDialogComponent;
  let fixture: ComponentFixture<TeamOperationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamOperationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamOperationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
