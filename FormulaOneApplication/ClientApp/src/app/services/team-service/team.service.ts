import {Injectable} from '@angular/core';
import {BaseService} from '../base-service/base.service';
import {AppConfig} from '../../config/config';
import {Helpers} from '../../helpers/helper';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {Team} from '../../models/team';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TeamService extends BaseService {

  private pathAPI = this.config.settings.PathAPI;
  private specAPI = 'team';
  // tslint:disable-next-line:variable-name
  private _singleTeam = new Subject<Team>();
  // tslint:disable-next-line:variable-name
  private _allTeams = new Subject<Team[]>();

  constructor(private http: HttpClient, private config: AppConfig, helper: Helpers) {
    super(helper);
  }

  getTeam(id: number): Observable<Team> {
    const singleTeam = this.http.get<Team>(this.pathAPI + this.specAPI + '/' + id, super.header()).pipe(
      catchError(super.handleError)
    );
    singleTeam.subscribe(value => this._singleTeam.next(value));

    return singleTeam;
  }

  getTeams(): Observable<Team[]> {
    const allTeams = this.http.get<Team[]>(this.pathAPI + this.specAPI, super.header()).pipe(
      catchError(super.handleError)
    );
    allTeams.subscribe(value => this._allTeams.next(value));

    return allTeams;
  }

  delete(id: number) {
    return this.http.delete<Team>(this.pathAPI + this.specAPI + '/' + id, super.header()).pipe(
      catchError(super.handleError)
    );
  }

  addTeam(team: Team) {
    return this.http.post<Team>(this.pathAPI + this.specAPI, team, super.header()).pipe(
      catchError(super.handleError)
    );
  }

  setTeam(team: Team) {
    return this.http.post<Team>(this.pathAPI + this.specAPI + '/' + team.id, team, super.header()).pipe(
      catchError(super.handleError)
    );
  }

  get singleTeam(): Observable<Team> {
    return this._singleTeam.asObservable();
  }

  get allTeams(): Observable<Team[]> {
    return this._allTeams.asObservable();
  }
}
