using System;
using FormulaOneApplication.Models;
using FormulaOneApplication.Models.Contexts;
using FormulaOneApplication.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;

namespace FormulaOneApplication.Data
{
    public static class ApplicationDbInitializer
    {
        public static void SeedAdminUser(UserManager<IdentityUser> userManager, bool hasRole = false)
        {
            IdentityUser user = new IdentityUser
            {
                UserName = "admin"
            };

            IdentityResult result = userManager.CreateAsync(user, "f1test2018").Result;

            if (hasRole && result.Succeeded)
            {
                userManager.AddToRoleAsync(user, "Admin").Wait();
            }
        }

        private static async void SeedAdminRole(RoleManager<IdentityRole> roleManager)
        {
            var roleExist = await roleManager.RoleExistsAsync("Admin");
            if (!roleExist)
            {
                roleManager.CreateAsync(new IdentityRole("Admin")).Wait();
            }
        }

        public static void SeedIdentityAdminWithRole(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            SeedAdminRole(roleManager);
            SeedAdminUser(userManager, true);
        }
        
        public static void Initialize(FOneTeamDbContext context)
        {
            context.Database.EnsureCreated();
            
            if (context.FOneTeams.Any())
            {
                return;   // DB has been seeded
            }
            
            context.FOneTeams.AddRange(
                new FOneTeam
                    {TeamId = Guid.NewGuid().GetHashCode(), TeamName = "Alfa Romeo", FoundationYear = 1950, VictoriesNumber = 10, Subscribed = true},
                new FOneTeam
                    {TeamId = Guid.NewGuid().GetHashCode(), TeamName = "Ferrari", FoundationYear = 1950, VictoriesNumber = 235, Subscribed = true},
                new FOneTeam
                    {TeamId = Guid.NewGuid().GetHashCode(), TeamName = "Haas", FoundationYear = 2016, VictoriesNumber = 0, Subscribed = true}
            );
            context.SaveChanges();
        }
    }
}