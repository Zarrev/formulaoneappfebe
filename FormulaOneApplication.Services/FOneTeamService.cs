﻿using System.Collections.Generic;
using FormulaOneApplication.Models;
using FormulaOneApplication.Repositories.Interfaces;
using FormulaOneApplication.Services.Interfaces;

namespace FormulaOneApplication.Services
{
    public class FOneTeamService: IFOneTeamService
    {
        private readonly IFOneTeamRepository _repository;

        public FOneTeamService(IFOneTeamRepository repository)
        {
            _repository = repository;
        }

        public List<FOneTeam> GetElements()
        {
            return (List<FOneTeam>) _repository.GetElements();
        }

        public FOneTeam GetElementById(int elementId)
        {
            return _repository.GetElementById(elementId);
        }

        public void InsertElement(FOneTeam element)
        {
            _repository.InsertElement(element);
            _repository.Save();
        }

        public void DeleteElement(int elementId)
        {
            _repository.DeleteElement(elementId);
            _repository.Save();
        }

        public void UpdateElement(FOneTeam element)
        {
            _repository.UpdateElement(element);
            _repository.Save();
        }
    }
}