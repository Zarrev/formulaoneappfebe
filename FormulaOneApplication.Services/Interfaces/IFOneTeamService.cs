using FormulaOneApplication.Models;

namespace FormulaOneApplication.Services.Interfaces
{
    public interface IFOneTeamService: IBaseService<FOneTeam, int>
    {
        //formally interface
    }
}