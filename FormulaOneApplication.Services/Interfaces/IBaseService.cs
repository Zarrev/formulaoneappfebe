using System.Collections.Generic;

namespace FormulaOneApplication.Services.Interfaces
{
    public interface IBaseService<T, K>
    {
        List<T> GetElements();
        T GetElementById(K elementId);
        void InsertElement(T element);
        void DeleteElement(K elementId);
        void UpdateElement(T element);
    }
}