using System.Collections.Generic;
using FormulaOneApplication.Maps.Interfaces;
using FormulaOneApplication.Models;
using FormulaOneApplication.Services.Interfaces;
using FormulaOneApplication.ViewModels;

namespace FormulaOneApplication.Maps
{
    public class FOneTeamMap : IBaseMap<FOneTeamViewModel>
    {
        private readonly IFOneTeamService _service;

        public FOneTeamMap(IFOneTeamService service)
        {
            _service = service;
        }

        public FOneTeamViewModel GetElement(int elementViewModelId)
        {
            return DomainToViewModel(_service.GetElementById(elementViewModelId));
        }

        public List<FOneTeamViewModel> GetAll()
        {
            return DomainToViewModel(_service.GetElements());
        }

        public void Create(FOneTeamViewModel elementViewModel)
        {
            _service.InsertElement(ViewModelToDomain(elementViewModel));
        }

        public void Delete(int elementViewModelId)
        {
            _service.DeleteElement(elementViewModelId);
        }

        public void Update(FOneTeamViewModel elementViewModel)
        {
            _service.UpdateElement(ViewModelToDomain(elementViewModel));
        }

        private FOneTeam ViewModelToDomain(FOneTeamViewModel elementViewModel)
        {
            var model = new FOneTeam(elementViewModel.id, elementViewModel.name, elementViewModel.foundation,
                elementViewModel.victories, elementViewModel.subscribed);

            return model;
        }

        private FOneTeamViewModel DomainToViewModel(FOneTeam elementModel)
        {
            var viewModel = new FOneTeamViewModel(elementModel.TeamId, elementModel.TeamName,
                elementModel.FoundationYear, elementModel.FoundationYear, elementModel.Subscribed);

            return viewModel;
        }

        private List<FOneTeamViewModel> DomainToViewModel(List<FOneTeam> elementModels)
        {
            var viewModels = new List<FOneTeamViewModel>();

            foreach (var model in elementModels)
            {
                viewModels.Add(new FOneTeamViewModel(model.TeamId, model.TeamName, model.FoundationYear,
                    model.VictoriesNumber, model.Subscribed));
            }

            return viewModels;
        }
    }
}