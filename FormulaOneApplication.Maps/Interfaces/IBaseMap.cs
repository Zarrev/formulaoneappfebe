using System.Collections.Generic;

namespace FormulaOneApplication.Maps.Interfaces
{
    public interface IBaseMap<TViewModel>
    {
        TViewModel GetElement(int elementViewModelId);
        List<TViewModel> GetAll();
        void Create(TViewModel elementViewModel);
        void Delete(int elementViewModelId);
        void Update(TViewModel elementViewModel);
    }
}