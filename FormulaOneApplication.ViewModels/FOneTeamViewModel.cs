namespace FormulaOneApplication.ViewModels
{
    public class FOneTeamViewModel
    {
        public int id{ get; set; }
        public string name{ get; set; }
        public int foundation{ get; set; }
        public int victories{ get; set; }
        public bool subscribed{ get; set; }

        public FOneTeamViewModel(){}
        
        public FOneTeamViewModel(int id, string name, int foundation, int victories, bool subscribed)
        {
            this.id = id;
            this.name = name;
            this.foundation = foundation;
            this.victories = victories;
            this.subscribed = subscribed;
        }
    }
}